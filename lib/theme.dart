import 'package:flutter/material.dart'; // ignore_for_file: prefer_const_constructors

Color primaryColor = Color(0xff81CB67);
Color secondaryColor = Color(0xffF9B650);
Color primaryText = Color(0xff314728);
Color secondaryText = Color(0xffA9B0A6);
